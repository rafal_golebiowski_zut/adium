from sklearn import datasets
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
import pickle
import matplotlib.gridspec as gridspec
from math import ceil, sqrt
import time
from Classifiers import DecisionTree

def main():
    
    olivetti = datasets.fetch_olivetti_faces()
    glasses = np.genfromtxt('olivetti_glasses.txt', delimiter=',') .astype(int)
    yGlasses = np.zeros(olivetti.data.shape[0])
    yGlasses = yGlasses.astype(int)
    yGlasses[glasses] = 1 
    y = yGlasses
    
    
    #showSomeImages(olivetti.images, glasses)
    
    xTrain, xTest, yTrain, yTest = train_test_split(olivetti.data, y, test_size = 0.2, stratify=y, random_state=0)
    xMean = np.mean(xTrain, axis = 0)
    
    
    
    #L, V = PCA(xTrain)
    #pickleAll([L, V], "olivettiPCA.pkl")
    L, V = unpickleAll("olivettiPCA.pkl")
    
    
    
    
    
    n = 50
    xTrainPca = xTrain.dot(V[:, :n])
    xTestPca = xTest.dot(V[:, :n])

    dt = DecisionTree(subtreesFunction="greedySubtrees",
                    k = 2, penalties = np.linspace(0.01, 0.05, 50),
                    maxDepth=2)
    dt.fit(xTrainPca, yTrain)
    #np.set_printoptions(precision = 1)
    #print(dt.tree)
    #print(dt.tree.shape)
    print(dt.predict(xTrainPca[:10, :]))
    #showSomeImages(xTrain[:10, :])
    
    print("Train acc", dt.score(xTrainPca, yTrain))
    print("Test acc", dt.score(xTestPca, yTest))
    

def PCA(data, varianceSumRatio = None, components = None):
    t1 = time.time()
    cov = np.cov(data, rowvar = False)
    L, V = np.linalg.eig(cov)
    L = np.real(L)
    V = np.real(V)
    ordering = np.argsort(-L)
    L = L[ordering]
    V = V[:, ordering]
    
    LSum = np.sum(L)
    currentLSum = 0
    if varianceSumRatio is not None:
        for index, value in enumerate(L):
            currentLSum += value
            if(currentLSum/LSum >= varianceSumRatio):
                components = index
                break
    if(components is not None):
        L = L[:components]
        V = V[:, :components]
    
    t2 = time.time()
  
    print("Time: " + str(t2 - t1) + "s")
    return L, V   
   
 
def reconstructions(img, V, dims, xMean):
    L = np.dot((img - xMean), V)
    reconstrs = np.array([np.dot(L[:dim], V[:, :dim].T) + xMean for dim in dims])
    return reconstrs


def showSomeImages(images, indexes = None, asGrid = True, title = None):
    if indexes is None:
        indexes = range(len(images))
    shape = images[0].shape
    
    if(len(shape) == 1):
        imgSide = int(np.sqrt(shape))
        images = images.reshape(images.shape[0], imgSide, imgSide)
    fig = plt.figure()
    plt.gray()
    if(title is not None):
        fig.canvas.set_window_title(title)
    grid = int(np.ceil(np.sqrt(len(indexes))))
    for i, index in enumerate(indexes):
        if(asGrid):
            plt.subplot(grid, grid, i + 1)
        else:
            plt.subplot(1, len(indexes), i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.imshow(images[index])
        
    plt.show()

def getSubplotSize(n):
	rows = ceil(sqrt(n))
	cols = ceil((n) / rows)
	return rows, cols

def showImagePairs(originals, reconstrs, errors, title, subtitles):
    shape = reconstrs[0].shape
    if(len(shape) == 1):
        imgSide = int(np.sqrt(shape))
        reconstrs = reconstrs.reshape(reconstrs.shape[0], imgSide, imgSide)
        originals = originals.reshape(originals.shape[0], imgSide, imgSide)
    
    rows, cols = getSubplotSize(len(reconstrs))

    fig = plt.figure()
    plt.gray()
    fig.canvas.set_window_title(title)
    outer = gridspec.GridSpec(rows, cols)
    
    for i in range(len(reconstrs)):
        inner = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=outer[i])
        
        plt.subplot(outer[i])
        plt.gca().set_title(subtitles[i] + ". Err: " + format(errors[i], '.5e'))
        plt.gca().axis('off')
        
        ax1 = plt.Subplot(fig, inner[0])
        ax2 = plt.Subplot(fig, inner[1])
        ax1.imshow(originals[i])
        ax2.imshow(reconstrs[i])
        
        ax1.axis('off')
        ax2.axis('off')
        
        fig.add_subplot(ax1)
        fig.add_subplot(ax2)
        
    plt.show()


def pickleAll(someList, fileName):
    file = open(fileName, 'wb')
    pickle.dump(someList, file)
    file.close()
def unpickleAll(fileName):
    file = open(fileName, 'rb')
    someList = pickle.load(file)
    file.close()
    return someList
   
 
main()