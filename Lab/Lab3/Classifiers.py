from sklearn.base import BaseEstimator, ClassifierMixin
import numpy as np
import time

class DecisionTree(BaseEstimator, ClassifierMixin):
    COL_PARENT = 0
    COL_SPLIT_FEATURE = 1
    COL_SPLIT_VALUE = 2
    COL_CHILD_LEFT = 3
    COL_CHILD_RIGHT = 4
    COL_Y = 5
    COL_DEPTH = 6
    
    def __init__(self, impurity = "impurityEntropy", maxDepth = None, minNodeExamples = 0.0, 
                pruning = False, penalty = 0.01,
                subtreesFunction = "greedySubtrees",
                k = None, penalties = None):
        self.tree = None
        self.classLabels = None
        self.impurity = getattr(self, impurity)
        self.maxDepth = maxDepth
        self.minNodeExamples = minNodeExamples
        self.subtreesFunction = getattr(self, subtreesFunction)

        if(k is not None and penalties is not None and k > 0 and penalties.any()):
            self.k = k
            self.penalties = penalties
            self.pruning = True
            self.penalty = 0
        else:
            self.k = None
            self.penalties = None
            self.pruning = pruning
            self.penalty = penalty
        
        
    def bestSplit(self, X, y, indexes):
        n = X.shape[1]
        bestK = None
        bestV = None
        bestExpect = np.inf
        for k in range(n):
            XIndexesK = X[indexes, k]
            u = np.unique(XIndexesK)
            vals = 0.5 * (u[:-1] + u[1:])
            
            for v in vals:
                indexesLeft = indexes[np.where(XIndexesK < v)[0]]
                indexesRight = indexes[np.where(XIndexesK >= v)[0]]
                distrLeft = self.yDistribution(y, indexesLeft)
                distrRight = self.yDistribution(y, indexesRight)
                expect = (indexesLeft.size * self.impurity(distrLeft) + indexesRight.size * self.impurity(distrRight)) / indexes.size
                
                if(expect < bestExpect):
                    bestExpect = expect
                    bestK = k
                    bestV = v
            
        return bestK, bestV, bestExpect
        
        
    def growTree(self, X, y, indexes, nodeIndex, depth):
        if self.tree is None:
            self.tree = np.zeros((1, 7))
            self.tree[0, 0] = -1.0
        
        self.tree[nodeIndex, DecisionTree.COL_DEPTH] = depth
        yDistr = self.yDistribution(y, indexes)
        self.tree[nodeIndex, DecisionTree.COL_Y] = self.classLabels[np.argmax(yDistr)]
        imp = self.impurity(yDistr)
        #print('impurity: ', imp)
        
        if imp == 0.0 or (self.maxDepth is not None and depth == self.maxDepth) or indexes.size < self.minNodeExamples * X.shape[0]:
            return self.tree
        k, v, expect = self.bestSplit(X, y, indexes)
        if expect >= imp:
            return self.tree
        
        self.tree[nodeIndex, DecisionTree.COL_SPLIT_FEATURE] = k
        self.tree[nodeIndex, DecisionTree.COL_SPLIT_VALUE] = v
        nodesSoFar = self.tree.shape[0]
        self.tree[nodeIndex, DecisionTree.COL_CHILD_LEFT] = nodesSoFar
        self.tree[nodeIndex, DecisionTree.COL_CHILD_RIGHT] = nodesSoFar + 1
        self.tree = np.r_[self.tree, np.zeros((2, 7))]
        self.tree[nodesSoFar, DecisionTree.COL_PARENT] = nodeIndex
        self.tree[nodesSoFar + 1, DecisionTree.COL_PARENT] = nodeIndex
        
        XIndexesK = X[indexes, k]
        indexesLeft = indexes[np.where(XIndexesK < v)[0]]
        indexesRight = indexes[np.where(XIndexesK >= v)[0]]
        self.growTree(X, y, indexesLeft, nodesSoFar, depth + 1)
        self.growTree(X, y, indexesRight, nodesSoFar + 1, depth + 1)
        
        return self.tree
        
        
    def fit(self, X, y):
        self.classLabels = np.unique(y)
        self.growTree(X, y, np.arange(X.shape[0]), 0, 0)

        if(self.k is not None):
            self.crossValidation(X, y)
            print('Best penalty', self.penalty)

        if self.pruning:
            scores, subtrees = self.subtreesFunction(X, y)
            print('scores:', scores)
            print('scores count:', len(scores))
            
            bestScore = np.inf
            bestKey = None
            
            for key, value in scores.items():
                if value < bestScore:
                    bestScore = value
                    bestKey = key
            self.tree = subtrees[bestKey]
        return self
    
    def crossValidation(self, X, y):
        chunksIndexes = np.array_split(np.random.permutation(X.shape[0]), self.k)

        bestErrors = np.zeros((self.k, self.penalties.shape[0]))
        for i in range(self.k):
            XSub = np.delete(X, chunksIndexes[i], axis = 0)
            ySub = np.delete(y, chunksIndexes[i])
            tree = DecisionTree(penalty=0, 
                    impurity=self.impurity.__name__,
                    maxDepth=self.maxDepth,
                    minNodeExamples=self.minNodeExamples,
                    subtreesFunction=self.subtreesFunction.__name__)
            tree.fit(XSub, ySub)
            scores, subtrees = tree.subtreesFunction(XSub, ySub)

            trainScores = []
            testScores = []
            for key, subtree in subtrees.items():
                indexes = np.fromstring(key[1:-1], dtype=int, sep=' ')
                leaves = np.sum(subtree[indexes, DecisionTree.COL_CHILD_LEFT] == 0)
                trainScores.append(scores[key] + leaves * self.penalties)
                tree.tree = subtree
                testScores.append(1 - tree.score(X[chunksIndexes[i], :], y[chunksIndexes[i]]) + leaves * self.penalties)
            
            testScores = np.array(testScores)
            
            bestSubtreesIndexes = np.argmin(trainScores, axis = 0)
            
            bestErrors[i, :] = np.array(testScores[bestSubtreesIndexes, range(self.penalties.shape[0])])
        
        self.penalty = self.penalties[np.argmin(np.mean(bestErrors, axis = 0))]

    def exhaustiveSubtrees(self, X, y):
        treePrime = np.copy(self.tree)
        treePrime[0, DecisionTree.COL_CHILD_LEFT] = 0
        treePrime[0, DecisionTree.COL_CHILD_RIGHT] = 0
        scores = {}
        subtrees = {}
        return self.doExhaustiveSubtrees(X, y, treePrime, np.array([0]), scores, subtrees)
    
    def doExhaustiveSubtrees(self, X, y, treePrime, indexesPrime, scores, subtrees):
        err = 1.0 - np.sum(self.predict(X, treePrime) == y) / float(X.shape[0])
        leaves = np.sum(treePrime[indexesPrime, DecisionTree.COL_CHILD_LEFT] == 0)
        scores[str(indexesPrime)] = err + leaves * self.penalty
        subtrees[str(indexesPrime)] = treePrime
        
        leavesIndexesPrime = indexesPrime[(np.where(treePrime[indexesPrime, DecisionTree.COL_CHILD_LEFT] == 0.0)[0]).astype(int)]
        nonLeavesIndexes = (np.where(self.tree[:, DecisionTree.COL_CHILD_LEFT] > 0)[0]).astype(int)
        nodesToExtend = np.intersect1d(leavesIndexesPrime, nonLeavesIndexes).astype(int)
        
        for t in nodesToExtend:
            indexesBis = np.copy(indexesPrime)
            indexesBis = np.append(indexesBis, self.tree[t, DecisionTree.COL_CHILD_LEFT].astype(int))
            indexesBis = np.append(indexesBis, self.tree[t, DecisionTree.COL_CHILD_RIGHT].astype(int))
            indexesBis.sort()
            key = str(indexesBis)
            
            if key not in scores:
                treeBis = np.copy(treePrime)
                treeBis[t, DecisionTree.COL_CHILD_LEFT] = self.tree[t, DecisionTree.COL_CHILD_LEFT]
                treeBis[t, DecisionTree.COL_CHILD_RIGHT] = self.tree[t, DecisionTree.COL_CHILD_RIGHT]
                
                treeBis[treeBis[t, DecisionTree.COL_CHILD_LEFT].astype(int), DecisionTree.COL_CHILD_LEFT] = 0
                treeBis[treeBis[t, DecisionTree.COL_CHILD_LEFT].astype(int), DecisionTree.COL_CHILD_RIGHT] = 0
                treeBis[treeBis[t, DecisionTree.COL_CHILD_RIGHT].astype(int), DecisionTree.COL_CHILD_LEFT] = 0
                treeBis[treeBis[t, DecisionTree.COL_CHILD_RIGHT].astype(int), DecisionTree.COL_CHILD_RIGHT] = 0
                
                self.doExhaustiveSubtrees(X, y, treeBis, indexesBis, scores, subtrees)
            
        
        return scores, subtrees
    
    def greedySubtrees(self, X, y):
        treePrime = np.copy(self.tree)
        treePrime[0, DecisionTree.COL_CHILD_LEFT] = 0
        treePrime[0, DecisionTree.COL_CHILD_RIGHT] = 0
        scores = {}
        subtrees = {}
        return self.doGreedySubtrees(X, y, treePrime, np.array([0]), scores, subtrees)

    def doGreedySubtrees(self, X, y, treePrime, indexesPrime, scores, subtrees):
        err = 1.0 - np.sum(self.predict(X, treePrime) == y) / float(X.shape[0])
        leaves = np.sum(treePrime[indexesPrime, DecisionTree.COL_CHILD_LEFT] == 0)
        scores[str(indexesPrime)] = err + leaves * self.penalty
        subtrees[str(indexesPrime)] = treePrime
        
        leavesIndexesPrime = indexesPrime[(np.where(treePrime[indexesPrime, DecisionTree.COL_CHILD_LEFT] == 0.0)[0]).astype(int)]
        nonLeavesIndexes = (np.where(self.tree[:, DecisionTree.COL_CHILD_LEFT] > 0)[0]).astype(int)
        nodesToExtend = np.intersect1d(leavesIndexesPrime, nonLeavesIndexes).astype(int)
        
        if(nodesToExtend.shape[0] == 0):
            return scores, subtrees

        bisErrors = {}
        bisSubtreesIndexes = {}
        bisSubtrees = {}
        for t in nodesToExtend:
            indexesBis = np.copy(indexesPrime)
            indexesBis = np.append(indexesBis, self.tree[t, DecisionTree.COL_CHILD_LEFT].astype(int))
            indexesBis = np.append(indexesBis, self.tree[t, DecisionTree.COL_CHILD_RIGHT].astype(int))
            indexesBis.sort()
            key = str(indexesBis)
            

            treeBis = np.copy(treePrime)
            treeBis[t, DecisionTree.COL_CHILD_LEFT] = self.tree[t, DecisionTree.COL_CHILD_LEFT]
            treeBis[t, DecisionTree.COL_CHILD_RIGHT] = self.tree[t, DecisionTree.COL_CHILD_RIGHT]
            
            treeBis[treeBis[t, DecisionTree.COL_CHILD_LEFT].astype(int), DecisionTree.COL_CHILD_LEFT] = 0
            treeBis[treeBis[t, DecisionTree.COL_CHILD_LEFT].astype(int), DecisionTree.COL_CHILD_RIGHT] = 0
            treeBis[treeBis[t, DecisionTree.COL_CHILD_RIGHT].astype(int), DecisionTree.COL_CHILD_LEFT] = 0
            treeBis[treeBis[t, DecisionTree.COL_CHILD_RIGHT].astype(int), DecisionTree.COL_CHILD_RIGHT] = 0


            bisErrors[key] = 1.0 - np.sum(self.predict(X, treeBis) == y) / float(X.shape[0])
            bisSubtreesIndexes[key] = indexesBis
            bisSubtrees[key] = treeBis

                
        bestKey = min(bisErrors, key = bisErrors.get)        
        bestSubtreeIndexes = bisSubtreesIndexes[bestKey]  
        bestSubtree = bisSubtrees[bestKey]

        self.doGreedySubtrees(X, y, bestSubtree, bestSubtreeIndexes, scores, subtrees)
        return scores, subtrees
    

    def predictX(self, x, tree = None):
        tree = self.tree if tree is None else tree
        nodeIndex = 0
        while True:
            if tree[nodeIndex, DecisionTree.COL_CHILD_LEFT] == 0.0:
                return tree[nodeIndex, DecisionTree.COL_Y]
            k = int(tree[nodeIndex, DecisionTree.COL_SPLIT_FEATURE])
            v = tree[nodeIndex, DecisionTree.COL_SPLIT_VALUE]
            
            nodeIndex = int(tree[nodeIndex, DecisionTree.COL_CHILD_LEFT]) if x[k] < v else int(tree[nodeIndex, DecisionTree.COL_CHILD_RIGHT])
            
    
    def predict(self, X, tree = None):
        predictions = np.zeros(X.shape[0])
        for i in range(X.shape[0]):
            predictions[i] = self.predictX(X[i], tree)
        return predictions
        
        
    def yDistribution(self, y, indexes):
        distr = np.zeros(self.classLabels.size)
        yIndexes = y[indexes]
        for i, label in enumerate(self.classLabels):
            distr[i] = np.where(yIndexes == label)[0].size / float(indexes.size)
        return distr
        
    def impurityError(self, yDistr):
        return 1.0 - np.max(yDistr)
    
    def impurityEntropy(self, yDistr):
        yDistr = yDistr[yDistr > 0.0]
        return -(yDistr * np.log2(yDistr)).sum()
    
    def impurityGini(self, yDistr):
        return 1 - (yDistr**2).sum()
        