import numpy as np
from scipy.io import loadmat
from sklearn.svm import SVC
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from shapely.ops import nearest_points
from shapely.geometry import LineString, Point
#Importing with custom names to avoid issues with numpy / sympy matrix
from cvxopt import matrix as cvxopt_matrix
from cvxopt import solvers as cvxopt_solvers

D = loadmat('data_for_svm.mat')
X = D['X2'] 
Y = X[:, 3]
X = X[:, :-1]




#Initializing values and computing H. Note the 1. to force to float type
m,n = X.shape
y = Y.reshape(-1,1) * 1.
X_dash = y * X
H = np.dot(X_dash , X_dash.T) * 1.

#Converting into cvxopt format
P = cvxopt_matrix(H)
q = cvxopt_matrix(-np.ones((m, 1)))
G = cvxopt_matrix(-np.eye(m))
h = cvxopt_matrix(np.zeros(m))
A = cvxopt_matrix(y.reshape(1, -1))
b = cvxopt_matrix(np.zeros(1))

#Setting solver parameters (change default to decrease tolerance) 
cvxopt_solvers.options['show_progress'] = False
cvxopt_solvers.options['abstol'] = 1e-10
cvxopt_solvers.options['reltol'] = 1e-10
cvxopt_solvers.options['feastol'] = 1e-10

#Run solver
sol = cvxopt_solvers.qp(P, q, G, h, A, b)
alphas = np.array(sol['x'])

#w parameter in vectorized form
w = ((y * alphas).T @ X).reshape(-1,1)

#Selecting the set of indices S corresponding to non zero parameters
S = (alphas > 1e-4).flatten()

#Computing b
b = y[S] - np.dot(X[S], w)

#Display results
print('Alphas = ', alphas[alphas > 1e-8])
print('w = ', w.flatten())
print('b = ', b[0])



margin = 1 / np.sqrt(np.sum(w.flatten() ** 2))
supportVectors = X[alphas[:, 0] > 1e-8, :]


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#points
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=Y)
#lines
z = lambda x,y: (-b[0]-w[0][0]*x-w[1][0]*y) / w[2][0]
x,y = np.meshgrid([min(X[:, 0]), max(X[:, 0])], [min(X[:, 1]), max(X[:, 1])])
ax.plot_surface(x, y, z(x,y))
#supportLines
zUp = lambda x,y: (-b[0]-w[0][0]*x-w[1][0]*y - 1) / w[2][0]
zDown = lambda x,y: (-b[0]-w[0][0]*x-w[1][0]*y + 1) / w[2][0]
ax.plot_surface(x, y, zUp(x,y))
ax.plot_surface(x, y, zDown(x,y))
#supportVectors
ax.scatter(supportVectors[:, 0], supportVectors[:, 1], supportVectors[:, 2], c="r", marker="x", s = 60)
# #marginsFromLineToSupportVectors


print(f'margin: {margin}')
plt.show()
