import numpy as np
from scipy.io import loadmat
from sklearn.svm import SVC
from matplotlib import pyplot as plt
from shapely.ops import nearest_points
from shapely.geometry import LineString, Point
#Importing with custom names to avoid issues with numpy / sympy matrix
from cvxopt import matrix as cvxopt_matrix
from cvxopt import solvers as cvxopt_solvers

D = loadmat('data_for_svm.mat')
X = D['X1'] 
y = X[:, 2]
X = X[:, :-1]




#Initializing values and computing H. Note the 1. to force to float type
m,n = X.shape
y = y.reshape(-1,1) * 1.
X_dash = y * X
H = np.dot(X_dash , X_dash.T) * 1.

H = np.eye(n + 1, n + 1)
H[0, 0] = 0
f = np.zeros((n + 1, 1))
A = -np.hstack((np.ones((m, 1)), X)))
Y = np.tile(y, (1, n + 1))
A = Y * A
b = -np.ones((m, 1))
#Converting into cvxopt format
P = cvxopt_matrix(H)
q = cvxopt_matrix(f)
G = cvxopt_matrix(A)
h = cvxopt_matrix(b)
# A = cvxopt_matrix(y.reshape(1, -1))
# b = cvxopt_matrix(np.zeros(1))

#Setting solver parameters (change default to decrease tolerance) 
cvxopt_solvers.options['show_progress'] = False
cvxopt_solvers.options['abstol'] = 1e-10
cvxopt_solvers.options['reltol'] = 1e-10
cvxopt_solvers.options['feastol'] = 1e-10

#Run solver
sol = cvxopt_solvers.qp(P, q, G, h)
alphas = np.array(sol['x'])

#w parameter in vectorized form
w = ((y * alphas).T @ X).reshape(-1,1)

#Selecting the set of indices S corresponding to non zero parameters
S = (alphas > 1e-4).flatten()

#Computing b
b = y[S] - np.dot(X[S], w)

#Display results
print('Alphas = ',alphas[alphas > 1e-4])
print('w = ', w.flatten())
print('b = ', b[0])


a = -w[0]/w[1]
b = (b[0]) / w[1]

x = np.array([min(X[:, 0]), max(X[:, 0])])
y = a * x - b
margin = 1 / np.sqrt(np.sum(w.flatten() ** 2))
yDown = y - np.sqrt(1 + a ** 2) * margin
yUp = y + np.sqrt(1 + a ** 2) * margin


#points
plt.scatter(X[:, 0], X[:, 1], c=X[:, 2])
#lines
plt.plot(x, y)
#supportLines
plt.plot(x, yDown, "k--")
plt.plot(x, yUp, "k--")
# #supportVectors
# plt.scatter(svc.support_vectors_[:, 0], svc.support_vectors_[:, 1], c="r", marker="x")
# #marginsFromLineToSupportVectors
# line = LineString([(x[0], y[0]), (x[1], y[1])]) 
# for i in range(svc.support_vectors_.shape[0]):
#     support = svc.support_vectors_[i, :]
#     p4 = nearest_points(line, Point(support[0], support[1]))[0]
#     plt.plot([support[0], p4.x], [support[1], p4.y], "g")

print(f'margin: {margin}')
plt.show()
