import numpy as np
from scipy.io import loadmat
from sklearn.svm import SVC
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from shapely.ops import nearest_points
from shapely.geometry import LineString, Point, Polygon

D = loadmat('data_for_svm.mat')
X = D['X2'] 
Y = X[:, 3]
X = X[:, :-1]

svc = SVC(kernel='linear')
svc.fit(X, Y)

margin = 1 / np.sqrt(np.sum(svc.coef_ ** 2))



fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
#points
ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=Y)
#lines
z = lambda x,y: (-svc.intercept_[0]-svc.coef_[0][0]*x-svc.coef_[0][1]*y) / svc.coef_[0][2]
x,y = np.meshgrid([min(X[:, 0]), max(X[:, 0])], [min(X[:, 1]), max(X[:, 1])])
ax.plot_surface(x, y, z(x,y))
#supportLines
zUp = lambda x,y: (-svc.intercept_[0]-svc.coef_[0][0]*x-svc.coef_[0][1]*y - 1) / svc.coef_[0][2]
zDown = lambda x,y: (-svc.intercept_[0]-svc.coef_[0][0]*x-svc.coef_[0][1]*y + 1) / svc.coef_[0][2]
ax.plot_surface(x, y, zUp(x,y))
ax.plot_surface(x, y, zDown(x,y))
#supportVectors
ax.scatter(svc.support_vectors_[:, 0], svc.support_vectors_[:, 1], svc.support_vectors_[:, 2], c="r", marker="x", s = 60)
# #marginsFromLineToSupportVectors


print(f'margin: {margin}')
plt.show()
