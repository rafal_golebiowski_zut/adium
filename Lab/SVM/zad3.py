import numpy as np
from scipy.io import loadmat
from sklearn.svm import SVC
from matplotlib import pyplot as plt
from shapely.ops import nearest_points
from shapely.geometry import LineString, Point

D = loadmat('data_for_svm.mat')
X = D['X3'] 
Y = X[:, 2]
X = X[:, :-1]

for C in [0.1, 1, 10]:
    svc = SVC(C = C, kernel='linear')
    svc.fit(X, Y)

    w = svc.coef_[0]
    a = -w[0]/w[1]
    b = (svc.intercept_[0]) / w[1]

    x = np.array([min(X[:, 0]), max(X[:, 0])])
    y = a * x - b
    margin = 1 / np.sqrt(np.sum(svc.coef_ ** 2))
    yDown = y - np.sqrt(1 + a ** 2) * margin
    yUp = y + np.sqrt(1 + a ** 2) * margin


    #points
    plt.scatter(X[:, 0], X[:, 1], c=Y)
    #lines
    plt.plot(x, y)
    #supportLines
    plt.plot(x, yDown, "k--")
    plt.plot(x, yUp, "k--")
    #supportVectors
    plt.scatter(svc.support_vectors_[:, 0], svc.support_vectors_[:, 1], c="r", marker="x")
    #marginsFromLineToSupportVectors
    line = LineString([(x[0], y[0]), (x[1], y[1])]) 
    for i in range(svc.support_vectors_.shape[0]):
        support = svc.support_vectors_[i, :]
        p4 = nearest_points(line, Point(support[0], support[1]))[0]
        plt.plot([support[0], p4.x], [support[1], p4.y], "g")

    print(f'{C} margin: {margin}')
    plt.show()
