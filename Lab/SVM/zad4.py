import numpy as np
from scipy.io import loadmat
from sklearn.svm import SVC
from matplotlib import pyplot as plt
from shapely.ops import nearest_points
from shapely.geometry import LineString, Point

D = loadmat('data_for_svm.mat')
X = D['X3'] 
Y = X[:, 2]
X = X[:, :-1]


svc = SVC(kernel='rbf', C = 1000, gamma=0.001)
svc.fit(X, Y)

#w = svc.coef_[0]

x = np.array([min(X[:, 0]), max(X[:, 0])])
#margin = 1 / np.sqrt(np.sum(svc.coef_ ** 2))


#points
plt.scatter(X[:, 0], X[:, 1], c=Y)
#lines
XX, YY = np.mgrid[min(X[:, 0]):max(X[:, 0]):200j, min(X[:, 1]):max(X[:, 1]):200j]
Z = svc.decision_function(np.c_[XX.ravel(), YY.ravel()])
Z = Z.reshape(XX.shape)
# plt.contour(XX, YY, Z,, levels = 1)
plt.contour(XX, YY, Z, colors='k', levels=[-1, 0, 1],
           linestyles=['--', '-', '--'])

#print(f'margin: {margin}')
plt.show()
